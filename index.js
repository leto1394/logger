'use strict'
const winston = require('winston')
require('winston-logstash')

// logger.log('info', 'Hello simple log 222!')
// logger.info('Hello log with metas', { color: 'blue', geo: {country: 'France', city: 'Paris'}, path: require('path').parse(process.cwd()) })

/**
 * @param {Object} cfg
 * @param {String} cfg.label
 * @param {String} cfg.host
 * @param {Number} cfg.port
 * @param {Number} cfg.retries
 * @param {Array} cfg.transports winston transports
 */
const Logger = function (cfg) {
  cfg = cfg || {}
  // let logstashTransport = new (winston.transports.Logstash)({
  //   port: cfg.port || 28777,
  //   host: cfg.host || '192.168.5.23',
  //   max_connect_retries: cfg.retries || -1,
  //   node_name: cfg.label || require('path').parse(process.cwd()).name
  // })
  let transports = []
  if (!cfg.disableLogstash) {
    transports.push(new (winston.transports.Logstash)({
      port: cfg.port || 28777,
      host: cfg.host || '192.168.5.23',
      max_connect_retries: cfg.retries || -1,
      node_name: cfg.label || require('path').parse(process.cwd()).name
    }))
  }
  if (Array.isArray(cfg.transports) && cfg.transports.length) {
    transports = transports.concat(cfg.transports)
  }
  const logger = new (winston.Logger)({transports})
  return logger
}

module.exports = Logger
